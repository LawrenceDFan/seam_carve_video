# README #

This is an old project of mine to apply the seam-carving algorithm to video. It is designed to convert 16:9 video to 4:3 video. 
The algorithm is based off of the algorithm described in ["*Seam Carving for Content-Aware Image Resizing*"](http://www.faculty.idc.ac.il/arik/SCWeb/imret/imret.pdf) by Avidan et al.