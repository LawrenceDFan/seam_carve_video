from skimage import transform, color
import numpy as np
import sys
import cv2

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

def forward_energy(img, flag=False):
    height = img.shape[0]
    width = img.shape[1]
    I = color.rgb2gray(img)
    
    energy = np.zeros((height, width))
    m = np.zeros((height, width))
    
    U = np.roll(I, 1, axis=0)
    L = np.roll(I, 1, axis=1)
    R = np.roll(I, -1, axis=1)
    
    cU = np.abs(R - L)
    cL = np.abs(U - L) + cU
    cR = np.abs(U - R) + cU
    
    for i in range(1, height):
        mU = m[i-1]
        mL = np.roll(mU, 1)
        mR = np.roll(mU, -1)
        
        mULR = np.array([mU, mL, mR])
        cULR = np.array([cU[i], cL[i], cR[i]])
        mULR += cULR

        argmins = np.argmin(mULR, axis=0)
        m[i] = np.choose(argmins, mULR)
        energy[i] = np.choose(argmins, cULR)
        
    return energy

aspect_ratio = (4/3)
SURF_hessian_threshold = 500
keypoint_cost_coeff = 2.0

in_name = str(sys.argv[1])
out_name = in_name[:-4] + "_out.mp4"
video = cv2.VideoCapture(in_name)

frame_rate = video.get(cv2.CAP_PROP_FPS)
img_y = int(video.get(3))
img_x = int(video.get(4))
new_y = int(np.floor(img_x*aspect_ratio))
columns_to_remove = int(img_y - new_y)
frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
print("Original: %d, %d   Aspect ratio: %.3f" % (img_y, img_x, img_y/img_x))
print("New: %d, %d   Aspect ratio: %.3f" % (new_y, img_x, new_y/img_x))
print("Output name: %s" % (out_name))
print("Number of frames: %d" % (frames))

out_video = cv2.VideoWriter(out_name, 0x7634706d, frame_rate, (new_y, img_x)) #Should be MP4
frame_counter = 0
cost_matrix_old = np.zeros((new_y, img_x))
surf = cv2.xfeatures2d.SURF_create(SURF_hessian_threshold)

while video.isOpened():
    ret, frame = video.read()
    if ret == True:
        printProgressBar(frame_counter, frames, prefix = 'Progress:', suffix = 'Complete', length = 50)
        kp1, des1 = surf.detectAndCompute(frame.astype(np.uint8), None)
        cost_matrix = forward_energy(frame)
        if frame_counter > 0:
            cost_matrix = 0.85*cost_matrix + 0.15*cost_matrix_old
        cost_matrix[np.uint8(cv2.KeyPoint_convert(kp1))] *= keypoint_cost_coeff
        resized_frame = transform.seam_carve(frame, cost_matrix, 'vertical', columns_to_remove)
        resized_frame = resized_frame*255.0
        out_video.write(resized_frame.astype('uint8'))
        cost_matrix_old = np.copy(cost_matrix)
        frame_counter += 1
    else:
        break

video.release()
out_video.release()
print("\nConversion completed successfully")